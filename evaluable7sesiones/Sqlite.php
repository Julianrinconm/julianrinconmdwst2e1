<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');
include_once("Enlaces.php");
include_once("TipoEnlace.php");

//https://www.if-not-true-then-false.com/2012/php-pdo-sqlite3-example/
// http://www.solocodigoweb.com/blog/2013/11/26/como-trabajar-con-sqlite-con-php/

class Sqlite {

    protected $conexion;

    public function __construct() {

        // sudo apt install php7.0-sqlite3
        //$path = "/home/paco/www/dws/pacoaldariasdws/";
        $nombre = Config::$bdnombre . '.db';
        $fichero = $path . $nombre;
        //$fichero = Config::$bdnombre . '.db';
        $this->conexion = NULL;

        try {
            $conexion = new PDO('sqlite:' . $fichero);
            $conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->conexion = $conexion;
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }

    public function getEnlaces() {

        $enlaces = array();
        $consulta = "SELECT id, nombre, url, tipoenlace "
          . "FROM enlaces  ORDER BY id";
        //echo $consulta;

        $result = $this->conexion->query($consulta);

        $cont = 0;
        //print_r($result);
        $filas = array();

        $filas = $result->fetchAll(PDO::FETCH_OBJ);
        //$filas = $result->fetchAll();
        if (!$filas) {
            die("Execute query error, because: " . print_r($this->conexion->errorInfo(), true));
        } else {

            foreach ($filas as $fila) {
                //echo $fila->id . "<br>";
                $tipoenlace = new TipoEnlace($fila->tipoenlace, "");
                //$enlace = new Enlaces($fila->id, $fila->nombre, $fila->url, $tipoenlace);
                $enlaces [$cont] = new Enlaces($fila->id, $fila->nombre, $fila->url, $tipoenlace);
                //$enlaces [$cont] = new Enlaces($fila->id, $fila->nombre, $fila->url);
                $cont++;
            }

            $conexion = false;
        }
        return $enlaces;
    }

    function grabarEnlace($enlace) {

        // Dar permiso de escritura: chmod 777 ceedcv.db


        try {
            $consulta = 'INSERT INTO enlaces ( id, nombre, url, tipoenlace ) '
              . 'VALUES( :id,  :nombre, :url, :tipoenlace);';

            //echo $consulta . "<br\>";
            //print_r($enlace);
            echo "<br\>";
            echo "Grabando: " . $enlace->getId() . " " . $enlace->getNombre()
            . " " . $enlace->getUrl() . " " . $enlace->getTipoenlace()->getId();

            $result = $this->conexion->prepare($consulta);

            if ($result) {
                echo "<br>";
                //echo "\nPDO::errorInfo():<br>\n";
                //print_r($result);
                //print_r($this->conexion);
            }


            /* Ejecuta una sentencia preparada pasando un array de valores */
            $count = $result->execute(array(
              ":id" => $enlace->getId()
              , ":nombre" => $enlace->getNombre()
              , ":url" => $enlace->getUrl()
              , ":tipoenlace" => $enlace->getTipoenlace()->getId()
              )
            );

            //echo "Count: " . $count . "<br>";
            //print_r($result->errorInfo());

            $this->conexion = false;
            return true;
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            return false;
        }
    }

    function instalar() {


        $fichero = Config::$bdnombre . ".db";
        echo "<h2>Creando BD Sqlite: " . $fichero . "</h2>";
        try {

            // Borrado del fichero si existe
            if (is_file($fichero)) {
                unlink($fichero);
            }

            // Crea el fichero o bd
            $this->conexion = new SQLite3($fichero);
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            return;
        }

        try {
//creamos la tabla profesor
            $sql = 'CREATE TABLE IF NOT EXISTS enlaces (
            id int(3) NOT NULL,
            nombre varchar(80) ,
            url varchar(80),
            tipoenlace int(3),
            PRIMARY KEY (id));';

            echo $sql . "<br>";

            $crear_tb_profesor = $conexion->prepare($sql);
            $crear_tb_profesor->execute();
            echo "Creada tabla: enlaces<br>";
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            return;
        }

        try {
            $sql = 'CREATE TABLE IF NOT EXISTS tipoenlace (
                id int(11) NOT NULL ,
		nombre varchar(100) NOT NULL
                );';
            echo $sql . "<br>";
            $crear_tb_asignatura = $conexion->prepare($sql);
            $crear_tb_asignatura->execute();
            echo "Creada tabla: tipoenlace<br>";
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            return;
        }

        try {
            $sql = "ALTER TABLE enlaces ADD CONSTRAINT fk_enlace_tipoenlace FOREIGN KEY (tipoenlace) references tipoenlace(id);";
            $fk = $conexion->prepare($sql);
            $fk->execute();
            echo $sql . "<br>";
            echo "Creada CAj: Enlace.idp -> TipoEnlace.id <br>";
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            return;
        }

        $conexion = false;
    }

}

?>