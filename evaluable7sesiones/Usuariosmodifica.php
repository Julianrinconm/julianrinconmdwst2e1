<?php
include_once('Mysql.php');
include_once('Config.php');
include_once("funciones.php");
error_reporting(E_ALL);
ini_set('display_errors', '1');
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title> <?php echo titulo(); ?></title>
    </head>
    <body>
        <?php

        function leer() {
            $id = recoge("id");
            $nombre = recoge("nombre");
            $tipousuario = recoge("tipousuario");
            $tipousuario = new TipoUsuario("1", "TipoUsuario");
            $user = recoge("usuario");
            $password = recoge("password");
            $email = recoge("email");
          $usuario = new Usuarios($id, $nombre, $tipousuario, $user, $password, $email);
          return $usuario;
        }

        //***************************
        //* Main
        //***************************
        $usuariomodificado = leer();

        

        if ($usuariomodificado->getId() != "" && $usuariomodificado->getNombre() != "") {
        $datos = new Mysql();
          $datos->modificaUsuario($usuariomodificado);
        }
        echo "Modificado usuario. ";
        echo '<a href="UsuariosMenu.php">Seguir</a>';
        //header('Location: EnlacesMenu.php');
        pie();
        ?>
    </body>
</html>
