<?php
session_start();
if ($_SESSION['SesionValida'] == 0) {
    header("Location: sesiones.php");
}
include_once('Config.php');
include_once("funciones.php");

switch ($_SESSION['datos']) {
    case 'ficheros':
        include_once("Ficheros.php");
        $datos = new Ficheros();
        break;
    case 'mysql':
        include_once("Mysql.php");
        $datos = new Mysql();
        break;
    case 'postgres':
        include_once("Postgres.php");
        $datos = new Postgres();
        break;
    case 'sqlite':
        include_once("Sqlite.php");
        $datos = new Sqlite();
        break;
}

$datos->instalar();
?>


<html>
    <head>
        <title>Instalar bd</title>
    </head>
    <body>
        <p>
            Inicio > <a href = "../index.php" >Volver</a> > InstalarBB
        </p>

        <p>
            Finalizada de la instalación ...
        </p>

        <?php pie(); ?>

    </body>

</html>