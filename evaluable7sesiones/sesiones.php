<?php
session_start();
?>
<?php
include_once('Config.php');
include_once("funciones.php");
include_once('Mysql.php');
?>
<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    </head>
    <body>
        <?php
        error_reporting(E_ALL);
        ini_set('display_errors', '1');
        ?>

        <h2>Control de Acceso</h2>
        <h2>Tema 7. Sesiones</h2>
        <form method="POST" action="" >
            <table border="1">

                <tr>
                    <td>Usuario</td>
                    <td>
                        <input type="text" name="usuario" value="admin" default="admin">
                    </td>
                </tr>
                <tr>
                    <td>Password</td>
                    <td>
                        <input type="password" name="password" value="admin" default="admin">
                    </td>
                </tr>
                <tr>
                    <td>Fuente de datos</td>
                    <td>

                        <?php
                        echo '<select name="datos">';
                        $fuentes = array('ficheros', 'mysql', 'postgres', 'sqlite');
                        $string = "";
                        foreach ($fuentes as $i => $value) {
                            //<option selected>
                            $string = '<option ';

                            // Dejamos marcada la opcion elegida

                            if (isset($_REQUEST["datos"])) {
                                if ($value == $_REQUEST["datos"]) {
                                    $string .= ' selected';
                                }
                            }

                            $string .= '>';
                            $string .= $value;
                            $string .= '</option>';
                            echo $string;
                        }
                        echo '</select>';
                        ?>

                    </td>
                </tr>
                <tr>
                    <td><input type="submit"  name= "Enviar" value="Enviar"></td>
                    <td><input type="reset"   name= "Borrar" value="Borrar"></td>
                </tr>
            </table>
        </select>
    </form>

    <p>Documentación evaluable 7 Sesiones:</p>
    <ul><li><a href="https://docs.google.com/document/d/1_igu8QZqYmMzuHn_NjaS3jZVm6Z0pKYZmJ9bDCviBrI/edit?usp=sharing"  target="docu6">Doc</a> </li>
    </ul>

    <?php
    if (isset($_REQUEST["usuario"]) && isset($_REQUEST["password"])) {
        $datos = new Mysql();
        $usuarios = $datos->getUsuarios();
        foreach ($usuarios as $usuario) {
        if ($_REQUEST["usuario"] == $usuario->getUser() && $_REQUEST["password"] == $usuario->getPassword()) {
            $_SESSION['SesionValida'] = 1;
            $_SESSION['datos'] = $_REQUEST["datos"];
            echo 'Correcto. <a href=" ./index.php">Seguir</a>';
            break;
            //header("Location: ./index.php");
        } else {
            echo "Error: Usuario/Password incorrecto<br>";
            echo "Usuarios y pass en la BBDD son: " . $usuario->getUser() . " con " . $usuario->getPassword() . "<br>";
        }
    }
    }
    ?>
