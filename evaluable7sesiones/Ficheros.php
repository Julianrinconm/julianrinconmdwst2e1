<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');
include_once("Enlaces.php");
include_once("TipoEnlace.php");

class Ficheros {

    //**************************************
    public function getEnlaces() {
        $enlaces = array();

        $fe = "Enlaces.txt";
        $f = @fopen($fe, "r");

        if ($f) {
            $data = fgetcsv($f, 1000, ";");
            $cont = 0;
            while ($data) {
                $tipoenlace = new TipoEnlace($data[3], "");
                $enlace = new Enlaces($data[0], $data[1], $data[2], $tipoenlace);
                $enlaces[$cont] = $enlace;
                $cont++;
                $data = fgetcsv($f, 1000, ";");
            }
            fclose($f);

            //echo "Num prof " . count($enlaces) . "<br>";
            //print_r($enlaces);
        } else {
            //echo "Error: No se puede abrir: " . $this->fe . "<br>";
        }

        return $enlaces;
    }

    //**************************************
    function getEnlace($enlace_) {
        $fe = "Enlaces.txt";
        $f = fopen($fe, "r");
        $data = fgetcsv($f, 1000, ";");
        $enlace = new Enlaces("", "");

        while ($data) {
            $idp = $data[0];
            //echo "Prof : " . $idp . " ==  " . $enlace_->getId() . "?<br>";
            if ($idp == $enlace_->getId()) {
                $enlace->setId($data[0]);
                $enlace->setNombre($data[1]);
                $enlace->setUrl($data[2]);
                $enlace->setTipoenlace($data[3]);
                break;
            }

            $data = fgetcsv($f, 1000, ";");
        }
        fclose($f);
        //echo "Leido Objeto: " . $enlace->getId() . " " . $enlace->getNombre() . "<br>";
        return $enlace;
    }

    //**************************************
    function grabarEnlace($enlace) {

        $fe = "Enlaces.txt";
        $f = fopen($fe, "a");
        $linea = $enlace->getId()
          . ";" . $enlace->getNombre()
          . ";" . $enlace->getUrl()
          . ";" . $enlace->getTipoenlace()->getId()
          . "\r\n";
        fwrite($f, $linea);

        fclose($f);
    }

    //**************************************
    function borrarEnlace($enlace) {

        // Leo todos los enlaces en un vector
        $fe = "Enlaces.txt";
        $enlaces = array();
        $enlaces = getEnlaces();

        // Borro el fichero
        ulink($fe);

        // Grabo todos los enlace del vector.
        if (count($enlaces) > 0) {
            foreach ($enlaces as $enlace) {
                grabarEnlace($enlace);
            }
        }
    }

}

?>