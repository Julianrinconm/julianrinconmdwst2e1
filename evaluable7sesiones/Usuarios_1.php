<?php

class Usuarios {

//Propiedades
  private $id;
  private $nombre;
  private $tipousuario;
  private $user;
  private $password;
  private $email;

//Constructor

  public function __construct($id, $nombre, $tipousuario, $user, $password, $email) {
    $this->id = $id;
    $this->nombre = $nombre;
    $this->tipousuario = $tipousuario;
    $this->user = $user;
    $this->password = $password;
    $this->email = $email;
  }

//Metodos
  public function getId() {
    return $this->id;
  }

  public function setId($id) {
    $this->id = $id;
  }

  public function getNombre() {
    return $this->nombre;
  }

  public function setNombre($nombre) {
    $this->nombre = $nombre;
  }

  public function getTipo() {
    return $this->tipousuario;
  }

  public function setTipo($tipousuario) {
    $this->tipousuario = $tipousuario;
  }

  public function getUser() {
    return $this->user;
  }

  public function setUser($user) {
    $this->user = $user;
  }

  public function getPassword() {
    return $this->password;
  }

  public function setPassword($password) {
    $this->password = $password;
  }
  
  public function getEmail() {
    return $this->email;
  }

  public function setEmail($email) {
    $this->email = $email;
  }
}

?>
