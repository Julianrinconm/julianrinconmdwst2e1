<?php
include_once('Config.php');
include_once("funciones.php");
?>
<!DOCTYPE html>
<html>
    <head>
        <title> <?php echo titulo(); ?></title>
        <meta charset="UTF-8">
    </head>
    <body>
        <?php include_once("funciones.php"); ?>
        <?php include_once("Ficheros.php"); ?>
        <?php cabecera(); ?>

        <p>Gestión de Usuarios:</p>
        <ul>
            <li><a href="UsuarioFormulario.php">Alta </a> </li>
        </ul>

        <?php
        echo "<p>Listado:</p>";

        echo '<table border="1" with="100">';
        echo '<tr>';
        echo '<td>Id </td>';
        echo '<td>Nombre </td>';
        echo '<td>Tipo usuario </td>';
        echo '<td>Usuario</td>';
        echo '<td>Password</td>';
        echo '<td>Email</td>';
        echo '<td>Borrar</td>';
        echo '<td>Actualizar</td>';
        echo '</tr>';

        $datos = new Ficheros();
        $usuarios = $datos->getUsuarios();

        if (count($usuarios) > 0) {

          foreach ($usuarios as $usuario) {
            echo "<tr>\n";
            echo "<td>" . $usuario->getId() . "</td>\n";
            echo "<td>" . $usuario->getNombre() . "</td>\n";
            echo "<td>" . $usuario->getTipo() . "</td>\n";
            echo "<td>" . $usuario->getUser() . "</td>\n";
            echo "<td>" . $usuario->getPassword() . "</td>\n";
            echo "<td>" . $usuario->getEmail() . "</td>\n";       
            echo '<td> <a href="UsuariosBorrar.php?id=' . $usuario->getId() . '">Borrar </td>';
            echo '<td> <a href="UsuariosActualizar.php?id=' . $usuario->getId() . '">Actualizar </td>';
            echo "</tr>\n";
          }
        }

        echo "</table>";
        echo "<br/>";


        volver();
        pie();
        ?>

</html>
