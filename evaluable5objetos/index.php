<?php include_once("funciones.php"); ?>
<!DOCTYPE html>
<html>
    <head>
        <title> <?php echo titulo(); ?></title>
        <meta charset="UTF-8">
    </head>
    <body>

        <?php cabecera(); ?>
        <h3>TEMA 5. OBJETOS</h3>

        <p>Elegir:</p>
        <ul>
            <li><a href="EnlacesMenu.php">Profesor. Paco Aldarias. Gestión de enlaces</a> </li>
            <li><a href="TiposEnlaces.php">Alumno1. Gestión de tipos de enlaces </a> </li>
            <li><a href="UsuariosMenu.php">Alumno2. Julian Rincon. Gestión de usuarios</a> </li>
            <li><a href="TiposUsuariosMenu.php">Alumno3. Gestión de tipos de usuarios</a> </li>
        </ul>

        <p>Documentación por tema:</p>
        <ul><li><a href="https://docs.google.com/document/d/1Z6xuT5Q0BGdhK6hT7rvboaKAdCM26iZ1LUpOm9De0lI/edit?usp=sharing"  target="docu5">Doc</a> </li>
        </ul>

        <?php volver(); ?>
        <?php pie(); ?>

    </body>
</html>
