<?php
include_once('Ficheros.php');
include_once("funciones.php");
error_reporting(E_ALL);
ini_set('display_errors', '1');
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title> <?php echo titulo(); ?></title>
    </head>
    <body>
        <?php

        function leer() {
            $id = recoge("id");
            $nombre = recoge("nombre");
            $tipousuario_ = recoge("tipousuario");
            $user = recoge("usuario");
            $password = recoge("password");
            $email = recoge("email");

          $tipousuario = new TipoUsuario("1", "TipoUsuario");
          $usuario = new Usuarios($id, $nombre, $tipousuario, $user, $password, $email);
          return $usuario;
        }

        //***************************
        //* Main
        //***************************

        $usuario = leer();
        if ($usuario->getId() != "" && $usuario->getNombre() != "") {
          $fichero = new Ficheros();
          $fichero->grabarUsuario($usuario);
          echo "Grabado usuario. ";
          echo '<a href="UsuariosMenu.php">Seguir</a>';
          //echo "Grabado: " . $enlace->getNombre() . "<br>";
        } else {
          //echo "Error: Campos vacios" . "<br>";
        }

        //header('Location: EnlacesMenu.php');
        pie();
        ?>
    </body>
</html>
