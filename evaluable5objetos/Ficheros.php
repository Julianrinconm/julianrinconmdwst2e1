<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');
include_once("Enlaces.php");
include_once("Usuarios.php");

class Ficheros {

  private $fe = "Enlaces.txt";
  private $fa = "Tipoenlaces.txt";
  private $fu = "Usuarios.txt";

  //**************************************
  public function getEnlaces() {
    $enlaces = array();

    $f = @fopen($this->fe, "r");

    if ($f) {
      $data = fgetcsv($f, 1000, ";");
      $cont = 0;
      while ($data) {
        $enlace = new Enlaces($data[0], $data[1], $data[2], $data[3]);
        $enlaces[$cont] = $enlace;
        $cont++;
        $data = fgetcsv($f, 1000, ";");
      }
      fclose($f);

      //echo "Num prof " . count($enlaces) . "<br>";
      //print_r($enlaces);
    } else {
      //echo "Error: No se puede abrir: " . $this->fe . "<br>";
    }

    return $enlaces;
  }
  //**************************************  
  public function getUsuarios() {
    $usuarios = array();

    $f = @fopen($this->fu, "r");

    if ($f) {
      $data = fgetcsv($f, 1000, ";");
      $cont = 0;
      while ($data) {
        $usuario = new Usuarios($data[0], $data[1], $data[2], $data[3], $data[4], $data[5]);
        $usuarios[$cont] = $usuario;
        $cont++;
        $data = fgetcsv($f, 1000, ";");
      }
      fclose($f);

      //echo "Num prof " . count($enlaces) . "<br>";
      //print_r($enlaces);
    } else {
      //echo "Error: No se puede abrir: " . $this->fe . "<br>";
    }

    return $usuarios;
    }

  //**************************************
  function getEnlace($enlace_) {
    $f = fopen($this->fe, "r");
    $data = fgetcsv($f, 1000, ";");
    $enlace = new Enlaces("", "");

    while ($data) {
      $idp = $data[0];
      //echo "Prof : " . $idp . " ==  " . $enlace_->getId() . "?<br>";
      if ($idp == $enlace_->getId()) {
        $enlace->setId($data[0]);
        $enlace->setNombre($data[1]);
        $enlace->setUrl($data[2]);
        $enlace->setTipoenlace($data[3]);
        break;
      }

      $data = fgetcsv($f, 1000, ";");
    }
    fclose($f);
    //echo "Leido Objeto: " . $enlace->getId() . " " . $enlace->getNombre() . "<br>";
    return $enlace;
  }
  //**************************************
    function getUsuario($usuario_) {
    $f = fopen($this->fu, "r");
    $data = fgetcsv($f, 1000, ";");
    $usuario = new Usuarios("", "");

    while ($data) {
      $idp = $data[0];

      if ($idp == $usuario_->getId()) {
        $usuario->setId($data[0]);
        $usuario->setNombre($data[1]);
        $usuario->setTipo($data[2]);
        $usuario->setUser($data[3]);
        $usuario->setPassword($data[4]);
        $usuario->setEmail($data[5]);
        break;
      }

      $data = fgetcsv($f, 1000, ";");
    }
    fclose($f);
    //echo "Leido Objeto: " . $enlace->getId() . " " . $enlace->getNombre() . "<br>";
    return $usuario;
  }
  //**************************************
  function grabarEnlace($enlace) {


    $f = fopen($this->fe, "a");
    $linea = $enlace->getId()
            . ";" . $enlace->getNombre()
            . ";" . $enlace->getUrl()
            . ";" . $enlace->getTipoenlace()
            . "\r\n";
    fwrite($f, $linea);

    fclose($f);
  }
  //**************************************
  function grabarUsuario($usuario) {


    $f = fopen($this->fu, "a");
    $linea = $usuario->getId()
            . ";" . $usuario->getNombre()
            . ";" . $usuario->getTipo()
            . ";" . $usuario->getUser()
            . ";" . $usuario->getPassword()
            . ";" . $usuario->getEmail()
            . "\r\n";
    fwrite($f, $linea);

    fclose($f);
  }
  //**************************************
  function borrarEnlace($enlace) {

    // Leo todos los enlaces en un vector
    $enlaces = array();
    $enlaces = getEnlaces();

    // Borro el fichero
    ulink($this->fe);

    // Grabo todos los enlace del vector.
    if (count($enlaces) > 0) {
      foreach ($enlaces as $enlace) {
        grabarEnlace($enlace);
      }
    }
  }
function borrarUsuario($usuario_) {

    // Leo todos los enlaces en un vector
   // $usuarios = array();
    $usuarios = $this->getUsuarios();

    // Borro el fichero
    unlink($this->fu);

    // Grabo todos los enlace del vector.
    if (count($usuarios) > 0) {
      foreach ($usuarios as $usuario) {
        if ($usuario->getId() == $usuario_){
            continue;
        } else {
        $this->grabarUsuario($usuario);
        }
      }
    }
  }

}

?>