<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');
include_once("Config.php");
include_once("Enlaces.php");
include_once("TipoEnlace.php");

class Postgres {

    protected $conexion;

    public function __construct() {

        $this->conexion = NULL;

        try {

            $bdconexion = new PDO('pgsql:host=' . Config::$bdhostname . ';dbname='
              . Config::$bdnombre . ';charset=utf8', Config::$bdusuario, Config::$bdclave);
            $this->conexion = $bdconexion;
        } catch (PDOException $e) {

            echo 'ERROR: ' . $e->getMessage();
        }
    }

    public function getEnlaces() {

        $enlaces = array();
        $consulta = "SELECT id, nombre, url, tipoenlace "
          . "FROM enlaces as a ORDER BY a.id";
//echo $consulta;

        $result = $this->conexion->query($consulta);

        $cont = 0;
//print_r($result);
        $filas = array();

        $filas = $result->fetchAll(PDO::FETCH_OBJ);
        if (!$filas) {
            die("Execute query error, because: " . print_r($this->pdo->errorInfo(), true));
        } else {

            foreach ($filas as $fila) {
//echo $fila->id . "<br>";
                $tipoenlace = new TipoEnlace("1", "Educacion");
//$enlace = new Enlaces($fila->id, $fila->nombre, $fila->url, $tipoenlace);
                $enlaces [$cont] = new Enlaces($fila->id, $fila->nombre, $fila->url, $tipoenlace);
//$enlaces [$cont] = new Enlaces($fila->id, $fila->nombre, $fila->url);
                $cont++;
            }

            $conexion = false;
        }
        return $enlaces;
    }

    function grabarEnlace($enlace) {


        $consulta = 'INSERT INTO enlaces ( id, nombre, url, tipoenlace ) '
          . 'VALUES( :id, :nombre, :url, :tipoenlace);
            ';

//echo $consulta . "<br\>";
//print_r($enlace);
        echo "<br\>";
        echo "Grabando: " . $enlace->getId() . " " . $enlace->getNombre()
        . " " . $enlace->getUrl() . " " . $enlace->getTipoenlace()->getId();

        $result = $this->conexion->prepare($consulta);

        if ($result) {
            echo "<br>";
//echo "\nPDO::errorInfo():<br>\n";
//print_r($result);
//print_r($this->conexion);
        }


        /* Ejecuta una sentencia preparada pasando un array de valores */
        $count = $result->execute(array(
          ":id" => $enlace->getId()
          , ":nombre" => $enlace->getNombre()
          , ":url" => $enlace->getUrl()
          , ":tipoenlace" => $enlace->getTipoenlace()->getId()
          )
        );

//echo "Count: " . $count . "<br>";
//print_r($result->errorInfo());

        $this->conexion = false;

        if ($count == 1) {
            return true;
        } else {
            return false;
        }
    }

    function instalar() {

        echo "<h2>Instalando: " . Config::$modelo . "</h2>";




        try {
//creamos la tabla profesor
            $sql = 'CREATE TABLE IF NOT EXISTS enlaces (
            id int(3) NOT NULL,
            nombre varchar(80) COLLATE utf8_spanish_ci,
            url varchar(80),
            tipoenlace int(3),
            PRIMARY KEY (id));
            ';

            echo $sql . "<br>";

            $crear_tb_profesor = $this->conexion->prepare($sql);
            $crear_tb_profesor->execute();
            echo "Creada tabla: enlaces<br>";
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            return;
        }

        try {
            $sql = 'CREATE TABLE IF NOT EXISTS tipoenlace (
            id int(11) NOT NULL,
            nombre varchar(100) COLLATE utf8_spanish_ci NOT NULL
            );
            ';
            echo $sql . "<br>";
            $crear_tb_asignatura = $this->conexion->prepare($sql);
            $crear_tb_asignatura->execute();
            echo "Creada tabla: tipoenlace<br>";
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            return;
        }

        try {
            $sql = "ALTER TABLE enlaces ADD CONSTRAINT fk_enlace_tipoenlace FOREIGN KEY (tipoenlace) references tipoenlace(id);";
            $fk = $this->conexion->prepare($sql);
            $fk->execute();
            echo $sql . "<br>";
            echo "Creada CAj: Enlace.idp -> TipoEnlace.id <br>";
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            return;
        }

        $conexion = false;
    }

}

?>