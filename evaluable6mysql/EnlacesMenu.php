<?php
include_once('Config.php');
include_once("funciones.php");
include_once("Ficheros.php");
include_once("Mysql.php");
include_once("Postgres.php");
include_once("Enlaces.php");

switch (Config::$modelo) {
    case 'fichero':
        include_once('Ficheros.php');
        $datos = new Ficheros();
        break;
    case 'mysql':
        include_once('Mysql.php');
        $datos = new Mysql();
        break;
    case 'postgres':
        include_once('Postgres.php');
        $datos = new Postgres();
        break;
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title> <?php echo titulo(); ?></title>
        <meta charset="UTF-8">
    </head>
    <body>

        <?php cabecera(); ?>

        <h2>Tema6. BBDD. Paco Aldarias</h2>

        <h3>Gestión de Enlaces:</h3>

        <p>
            Inicio > <a href = "../index.php" >  Tema6  </a> > Enlaces
        </p>


        <ul>
            <li><a href="EnlaceFormulario.php">Alta </a> </li>
        </ul>

        <?php
        echo "<p>Listado:</p>";

        echo '<table border="1" with="100">';
        echo '<tr>';
        echo '<td>Id </td>';
        echo '<td>Nom </td>';
        echo '<td>Url </td>';
        echo '<td>TipoUrl</td>';
        echo '<td>Borrar</td>';
        echo '<td>Actualizar</td>';
        echo '</tr>';


        $enlaces = $datos->getEnlaces();

        if (count($enlaces) > 0) {

            foreach ($enlaces as $enlace) {

                echo "<tr>\n";
                echo "<td>" . $enlace->getId() . "</td>\n";
                echo "<td>" . $enlace->getNombre() . "</td>\n";
                echo "<td>" . $enlace->getUrl() . "</td>\n";
                echo "<td>" . $enlace->getTipoenlace()->getId() . "</td>\n";
                echo '<td> <a href="EnlacesBorrar.php?id=' . $enlace->getId() . '">Borrar </td>';
                echo '<td> <a href="EnlacesActualizar.php?id=' . $enlace->getId() . '">Actualizar </td>';
                echo "</tr>\n";
            }
        }

        echo "</table>";
        echo "<br/>";

        pie();
        ?>

</html>
