<?php
include_once("Config.php");
include_once("funciones.php");
include_once("Enlaces.php");
include_once("TipoEnlace.php");

switch (Config::$modelo) {
    case 'fichero':
        include_once('Ficheros.php');
        $datos = new Ficheros();
        break;
    case 'mysql':
        include_once('Mysql.php');
        $datos = new Mysql();
        break;
    case 'postgres':
        include_once('Postgres.php');
        $datos = new Postgres();
        break;
}
error_reporting(E_ALL);
ini_set('display_errors', '1');
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title> <?php echo titulo(); ?></title>
    </head>
    <body>

        <h2>Tema6. Mysql. Paco Aldarias</h2>
        <h3>Gestión de Enlaces:</h3>

        <p>
            Inicio > Tema6 > <a href = "../EnlacesMenu.php" > Enlaces </a> > Alta
        </p>

        <?php

        function leer() {
            $id = recoge("id");
            $nombre = recoge("nombre");
            $url = recoge("url");
            $tipoenlace_ = recoge("tipoenlace");

            $tipoenlace = new TipoEnlace("1", "TipoEnlace");

            $enlace = new Enlaces($id, $nombre, $url, $tipoenlace);
            return $enlace;
        }

        //***************************
        //* Main
        //***************************



        $enlace = leer();
        if ($enlace->getId() != "" && $enlace->getNombre() != "") {

            $datos->grabarEnlace($enlace);
            echo "Grabado enlace. ";
            echo '<a href="EnlacesMenu.php">Seguir</a>';
            //echo "Grabado: " . $enlace->getNombre() . "<br>";
        } else {
            //echo "Error: Campos vacios" . "<br>";
        }

        //header('Location: EnlacesMenu.php');
        pie();
        ?>
    </body>
</html>
