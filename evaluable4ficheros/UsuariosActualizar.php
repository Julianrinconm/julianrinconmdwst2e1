<?php
include_once('Ficheros.php');
include_once('Config.php');
include_once("funciones.php");
?>

<html lang="es">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title> <?php echo titulo(); ?></title>
    </head>
    <body>
        <?php
        error_reporting(E_ALL);
        ini_set('display_errors', '1');
        ?>

        <?php cabecera();         
        $modifica = $_GET["id"];    
        $usuarios = getUsuarios();
        foreach ($usuarios as $usuario) {
            if($usuario[0] == $modifica){
            $valores=$usuario;
            break;
            } 
        }
        ?>
        <h3>Modificación Usuarios</h3>         
        <form action="Usuariosmodifica.php" method="post">
            <table>
                <?php echo '<input type="hidden" name="id" value="' . $modifica . '"/>' ; ?>
                <tr>
                    <td>Nombre</td>
                    <td><input type="text" name="nombre" value="<?php print $valores[1]; ?>"/></td>
                </tr>
                <tr>
                    <td>Tipo de usuario</td>
                    <td><input type="text" name="tipousuario" value="<?php print $valores[2]; ?>"/></td>
                </tr>
                <tr>
                    <td>Usuario</td>
                    <td><input type="text" name="usuario" value="<?php print $valores[3]; ?>"/></td>
                </tr>
                <tr>
                    <td>Password</td>
                    <td><input type="text" name="password" value="<?php print $valores[4]; ?>"/></td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td><input type="text" name="email" value="<?php print $valores[5]; ?>"/></td>
                </tr>                
            </table>

            <table>
                <tr>
                    <td>
                        <input type="submit" value="Enviar" />
                    </td>
                    <td>
                        <input type="reset" value="Borrar" />
                    </td>
                </tr>
            </table>
        </form>



        <?php volver(); ?>
        <?php pie(); ?>

    </body>
</html>