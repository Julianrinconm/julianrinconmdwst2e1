<?php
include_once('Ficheros.php');
include_once("funciones.php");
error_reporting(E_ALL);
ini_set('display_errors', '1');
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title> <?php echo titulo(); ?></title>
    </head>
    <body>
        <?php

        function leer() {
            $id = recoge("id");
            $nombre = recoge("nombre");
            $tipousuario = recoge("tipousuario");
            $user = recoge("usuario");
            $password = recoge("password");
            $email = recoge("email");
            $usuario[0] = $id;
            $usuario[1] = $nombre;
            $usuario[2] = $tipousuario;
            $usuario[3] = $user;
            $usuario[4] = $password;
            $usuario[5] = $email;  
            return $usuario;
        }

        //***************************
        //* Main
        //***************************

        $usuario = leer();

        if ($usuario[0] != "" && $usuario[1] != "") {

            grabarUsuario($usuario);
            echo "Grabado usuario. ";
            echo '<a href="UsuariosMenu.php">Seguir</a>';
            //echo "Grabado: " . $usuario->getNombre() . "<br>";
        } else {
            //echo "Error: Campos vacios" . "<br>";

        }

        //header('Location: EnlacesMenu.php');
        pie();
        ?>
    </body>
</html>
