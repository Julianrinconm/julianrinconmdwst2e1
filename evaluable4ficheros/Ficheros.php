<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');
$fa = "Tipoenlaces.txt";

//**************************************
function getEnlaces() {
    $enlaces = array();
    $enlace = array();

    $fe = "Enlaces.txt";
    $f = fopen($fe, "r");
    if ($f) {
        $data = fgetcsv($f, 1000, ";");
        $cont = 0;
        while ($data) {
            $enlace[0] = $data[0];
            $enlace[1] = $data[1];
            $enlace[2] = $data[2];
            $enlace[3] = $data[3];
            $enlaces[$cont] = $enlace;
            $cont++;
            $data = fgetcsv($f, 1000, ";");
        }
        fclose($f);

        return $enlaces;
    }
}

//**************************************
function grabarEnlace($enlace) {

    $fe = "Enlaces.txt";
    $f = fopen($fe, "a");
    $linea = $enlace[0]
      . ";" . $enlace[1]
      . ";" . $enlace[2]
      . ";" . $enlace[3]
      . "\r\n";
    fwrite($f, $linea);

    fclose($f);
}
function getUsuarios() {
    $usuarios = array();
    $usuario = array();
    $fe = "Usuarios.txt";
    $f = fopen($fe, "r");
    if ($f) {
        $data = fgetcsv($f, 1000, ";");
        $cont = 0;
        while ($data) {
            $usuario[0] = $data[0];
            $usuario[1] = $data[1];
            $usuario[2] = $data[2];
            $usuario[3] = $data[3];
            $usuario[4] = $data[4];
            $usuario[5] = $data[5];
            $usuarios[$cont] = $usuario;
            $cont++;
            $data = fgetcsv($f, 1000, ";");
        }
        fclose($f);
        return $usuarios;
    }
}
# Borro el archivo lo primero
function borraUsuarios() {
    $fe = "Usuarios.txt";
    $f = fopen($fe, "w");
    fwrite($f,"");
    fclose($f);
}
//**************************************
function grabarUsuario($usuario) {
    $fe = "Usuarios.txt";
    $f = fopen($fe, "a");
    $linea = $usuario[0]
      . ";" . $usuario[1]
      . ";" . $usuario[2]
      . ";" . $usuario[3]
      . ";" . $usuario[4]
      . ";" . $usuario[5]
      . "\r\n";
    fwrite($f, $linea);
    fclose($f);
}
?>
