<?php
include_once('Ficheros.php');
include_once("funciones.php");
error_reporting(E_ALL);
ini_set('display_errors', '1');
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title> <?php echo titulo(); ?></title>
    </head>
    <body>
        <?php
        $borro = $_GET["id"];
        //***************************
        //* Main
        //***************************
        $usuarios = getUsuarios();
        borraUsuarios();
        foreach ($usuarios as $usuario) {
            if($usuario[0] == $borro){
                continue;
            } else {
            grabarUsuario($usuario);
        }
        }
        echo "Borrado usuario. ";
        echo '<a href="UsuariosMenu.php">Seguir</a>';
        //header('Location: EnlacesMenu.php');
        pie();
        ?>
    </body>
</html>
