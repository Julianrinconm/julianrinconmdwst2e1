<?php
include_once('Config.php');
include_once("funciones.php");
include_once("Ficheros.php");
?>
<!DOCTYPE html>
<html>
    <head>
        <title> <?php echo titulo(); ?></title>
        <meta charset="UTF-8">
    </head>
    <body>

        <?php cabecera(); ?>

        <p>Gestión de Usuarios:</p>
        <ul>
            <li><a href="UsuarioFormulario.php">Alta </a> </li>
        </ul>

        <?php
        echo "<p>Listado:</p>";

        echo '<table border="1" with="100">';
        echo '<tr>';
        echo '<td>Id </td>';
        echo '<td>Nombre </td>';
        echo '<td>Tipo usuario </td>';
        echo '<td>Usuario</td>';
        echo '<td>Password</td>';
        echo '<td>Email</td>';
        echo '<td>Borrar</td>';
        echo '<td>Actualizar</td>';
        echo '</tr>';



        $usuarios = getUsuarios();

        if (count($usuarios) > 0) {

            foreach ($usuarios as $usuario) {
                echo "<tr>\n";
                echo "<td>" . $usuario[0] . "</td>\n";
                echo "<td>" . $usuario[1] . "</td>\n";
                echo "<td>" . $usuario[2] . "</td>\n";
                echo "<td>" . $usuario[3] . "</td>\n";
                echo "<td>" . $usuario[4] . "</td>\n";
                echo "<td>" . $usuario[5] . "</td>\n";
                echo '<td> <a href="UsuariosBorrar.php?id=' . $usuario[0] . '">Borrar </td>';
                echo '<td> <a href="UsuariosActualizar.php?id=' . $usuario[0] . '">Actualizar </td>';
                echo "</tr>\n";
            }

        }

        echo "</table>";
        echo "<br/>";


        volver();
        pie();
        ?>

</html>
