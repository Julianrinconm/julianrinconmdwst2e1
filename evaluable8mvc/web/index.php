<?php

// web/index.php
// carga del modelo y los controladores
require_once __DIR__ . '/../app/Config.php';
require_once __DIR__ . '/../app/Controller.php';
//require_once __DIR__ . '/../app/Enlaces.php';
require_once __DIR__ . '/../app/modelo/IModelo.php';


// enrutamiento
$map = array(
  'login' => array('controller' => 'Controller', 'action' => 'login'),
  'inicio' => array('controller' => 'Controller', 'action' => 'inicio'),
  'enlaces' => array('controller' => 'ControllerEnlaces', 'action' => 'inicio'),
  'tipoenlaces' => array('controller' => 'ControllerTipoEnlaces', 'action' => 'inicio'),
  'usuarios' => array('controller' => 'ControllerUsuarios', 'action' => 'inicio'),
  'tipousuarios' => array('controller' => 'ControllerTipoUsuarios', 'action' => 'inicio'),
  'salir' => array('controller' => 'Controller', 'action' => 'salir'),
);

// Parseo de la ruta
if (isset($_GET['ctl'])) {
    if (isset($map[$_GET['ctl']])) {
        $ruta = $_GET['ctl'];
    } else {
        header('Status: 404 Not Found');
        echo '<html><body><h1>Error 404: No existe la ruta <i>' .
        $_GET['ctl'] .
        '</p></body></html>';
        exit;
    }
} else {
    $ruta = 'inicio';
}

$controlador = $map[$ruta];
// Ejecución del controlador asociado a la ruta

if (method_exists($controlador['controller'], $controlador['action'])) {
    call_user_func(array(new $controlador['controller'], $controlador['action']));
} else {

    header('Status: 404 Not Found');
    echo '<html><body><h1>Error 404: El controlador <i>' .
    $controlador['controller'] .
    '->' .
    $controlador['action'] .
    '</i> no existe</h1></body></html>';
}