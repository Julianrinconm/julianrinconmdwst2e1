<?php
include_once('Config.php');
include_once("funciones.php");
?>
<!DOCTYPE html>
<html>
    <head>
        <title> <?php echo titulo(); ?></title>
        <meta charset="UTF-8">
    </head>
    <body>
        <?php include_once("funciones.php"); ?>
        <?php include_once("Ficheros.php"); ?>
        <?php cabecera(); ?>

        <p>Gestión de Enlaces:</p>
        <ul>
            <li><a href="EnlaceFormulario.php">Alta </a> </li>
        </ul>

        <?php
        echo "<p>Listado:</p>";

        echo '<table border="1" with="100">';
        echo '<tr>';
        echo '<td>Id </td>';
        echo '<td>Nom </td>';
        echo '<td>Url </td>';
        echo '<td>TipoUrl</td>';
        echo '<td>Borrar</td>';
        echo '<td>Actualizar</td>';
        echo '</tr>';

        $datos = new Ficheros();
        $enlaces = $datos->getEnlaces();

        if (count($enlaces) > 0) {

          foreach ($enlaces as $enlace) {
            echo "<tr>\n";
            echo "<td>" . $enlace->getId() . "</td>\n";
            echo "<td>" . $enlace->getNombre() . "</td>\n";
            echo "<td>" . $enlace->getUrl() . "</td>\n";
            echo "<td>" . $enlace->getTipoenlace() . "</td>\n";
            echo '<td> <a href="EnlacesBorrar.php?id=' . $enlace->getId() . '">Borrar </td>';
            echo '<td> <a href="EnlacesActualizar.php?id=' . $enlace->getId() . '">Actualizar </td>';
            echo "</tr>\n";
          }
        }

        echo "</table>";
        echo "<br/>";


        volver();
        pie();
        ?>

</html>
