<?php session_start(); ?>
<?php ob_start() ?>
<?php
if (!$_SESSION['SesionValida']) {
    header("Location: index.php");
}
?>



<!DOCTYPE html>
<html>
    <head>
        <title><?php echo Config::$titulo; ?></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>

        <?php if (isset($params['mensaje'])) : ?>
            <b><span style="color: red;"><?php echo $params['mensaje'] ?></span></b>
            <?php endif; ?>

        <?php
        $profesores = $params['profesores'];
        echo '<p><a href="' . __DIR__ . 'index.php?ctl=profesornuevo' . '">Nuevo</a></p>';
        echo '<table border="1" with="100">';
        echo '<tr>';
        echo '<td>Id Enlace</td>';
        echo '<td>Nom Enalces</td>';
        echo '<td>Borrar</td>';
        echo '</tr>';

        if (count($profesores) > 0) {
            foreach ($profesores as $profesor) {
                echo "<tr>";
                echo "<td>" . $profesor->getId() . "</td>";
                echo "<td>" . $profesor->getNombre() . "</td>";
                echo '<td><a href="' . __DIR__ . 'index.php?ctl=profesorborrar?id=' . $profesor->getId() . '">Borrar</a></td>';
                echo "</tr>";
            }
        }
        echo "</table>";
        echo "<br/>";
        ?>

        <?php $contenido = ob_get_clean() ?>

        <?php include 'layout.php' ?>
