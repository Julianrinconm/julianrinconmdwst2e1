<html>
    <head>
        <title><?php echo Config::$titulo ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="<?php echo 'css/' . Config::$mvc_vis_css ?>" />

    </head>
    <body>
        <div id="cabecera">
            <title><?php echo Config::$titulo ?></title>
            <h2>Práctica tema 8. MVC.</h2>
        </div>

        <div id="menu">
            <hr/>

            <a href="index.php?ctl=inicio">Inicio</a> |
            <a href="index.php?ctl=enlaces">Profesor-Enlaces</a> |
            <a href="index.php?ctl=tipoenlaces">Alumno1-Tipos Enlaces</a> |
            <a href="index.php?ctl=usuarios">Alumno2-Usuarios</a> |
            <a href="index.php?ctl=tiposusuarios">Alumno3-Tipos Usuarios</a> |
            <a href="../../index.php">Salir</a>
            <hr/>
        </div>

        <div id="contenido">
            <?php echo $contenido ?>
        </div>

        <div id="pie">
            <hr/>
            <div align="left">
                <?php
                echo "<pre>" . Config::$empresa . " " . Config::$grupo;
                echo Config::$curso . " " . Config::$fecha . "</pre>";
                ?>
            </div>
        </div>
    </body>
</html>
