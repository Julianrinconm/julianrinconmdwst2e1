<?php include_once("funciones.php"); ?>
<?php include_once("Config.php"); ?>
<!DOCTYPE html>
<html>
    <head>
        <title><?php echo Config::$titulo ?></title>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>

        <?php cabecera(); ?>


        <p>Proyecto: <?php echo Config::$tema; ?> </p>

        <p>Elegir:</p>
        <ul>
            <li><a href="ProfesorMenu.php">Gestión Profesor </a> </li>
            <li><a href="AsignaturaMenu.php">Gestión Asignatura </a> </li>
            <li><a href="InstalarAplicacion.php">Instalación</a> </li>
        </ul>

        <p>Documentación por tema:</p>
        <ul>
            <li><a href="https://drive.google.com/file/d/0B3UbpRtIyr6qRnRyemlmMERoZFk/view?usp=sharing.pdf">Tema5. POO. Enunciado </a> </li>
            <li><a href="https://drive.google.com/file/d/0B3UbpRtIyr6qelZZTEYwSlFEZ1E/view?usp=sharing">Tema6. MYSQL. Enunciado </a> </li>
        </ul>

        <p>Documentación de este proyecto:</p>
        <ul>
            <li><a href="DWSProyecto1Documentacion.pdf">Documentación </a> </li>
        </ul>


        <?php pie(); ?>

    </body>
</html>
