<?php

// app/Controller.php
error_reporting(E_ALL);
ini_set('display_errors', '1');

require_once __DIR__ . '/funciones.php';
require_once __DIR__ . '/modelo/IModelo.php';

class Controller {

    public function login() {
        $params = array(
          'mensaje' => 'Login',
          'fecha' => date('d-m-yyy'),
        );
        require __DIR__ . '/templates/login.php';
    }

    public function inicio() {
        $params = array(
          'mensaje' => 'Inicio',
          'fecha' => date('d-m-yyy'),
        );
        require __DIR__ . '/templates/inicio.php';
    }

    public function salir() {
        session_start();
        $_SESSION['SesionValida'] = 0;
        session_destroy();
        header("Location: index.php");
    }

}

?>
