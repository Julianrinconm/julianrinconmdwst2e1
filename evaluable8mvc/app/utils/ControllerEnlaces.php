<?php

// app/Controller.php
error_reporting(E_ALL);
ini_set('display_errors', '1');

require_once __DIR__ . '/funciones.php';
require_once __DIR__ . '/modelo/IModelo.php';

class Controller {

    public function inicio() {
        $params = array(
          'mensaje' => 'Inicio',
          'fecha' => date('d-m-yyy'),
        );
        require __DIR__ . '/templates/inicioEnlaces.php';
    }

    public function listar() {
        $m = new Model(Config::$mvc_bd_nombre, Config::$mvc_bd_usuario, Config::$mvc_bd_clave, Config::$mvc_bd_hostname);

        $params = array(
          'enlaces' => $m->dameEnlaces(),
        );

        require __DIR__ . '/templates/mostrarEnlaces.php';
    }

    public function insertar() {
        $params = array(
          'id' => '',
          'nombre' => '',
        );

        $m = new Model(Config::$mvc_bd_nombre, Config::$mvc_bd_usuario, Config::$mvc_bd_clave, Config::$mvc_bd_hostname);

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            // comprobar campos formulario
            if ($m->validarDatos($_POST['nombre'], $_POST['energia'], $_POST['proteina'], $_POST['hc'], $_POST['fibra'], $_POST['grasa'])) {
                $m->insertarAlimento($_POST['nombre'], $_POST['energia'], $_POST['proteina'], $_POST['hc'], $_POST['fibra'], $_POST['grasa']);
                header('Location: index.php?ctl=listar');
            } else {
                $params = array(
                  'nombre' => $_POST['nombre'],
                  'energia' => $_POST['energia'],
                  'proteina' => $_POST['proteina'],
                  'hc' => $_POST['hc'],
                  'fibra' => $_POST['fibra'],
                  'grasa' => $_POST['grasa'],
                );
                $params['mensaje'] = 'No se ha podido insertar el alimento. Revisa el formulario';
            }
        }

        require __DIR__ . '/templates/formInsertar.php';
    }

    public function buscarPorNombre() {
        $params = array(
          'nombre' => '',
          'resultado' => array(),
        );

        $m = new Model(Config::$mvc_bd_nombre, Config::$mvc_bd_usuario, Config::$mvc_bd_clave, Config::$mvc_bd_hostname);

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $params['nombre'] = $_POST['nombre'];
            $params['resultado'] = $m->buscarAlimentosPorNombre($_POST['nombre']);
        }

        require __DIR__ . '/templates/buscarPorNombre.php';
    }

    public function ver() {
        if (!isset($_GET['id'])) {
            throw new Exception('Página no encontrada');
        }

        $id = $_GET['id'];

        $m = new Model(Config::$mvc_bd_nombre, Config::$mvc_bd_usuario, Config::$mvc_bd_clave, Config::$mvc_bd_hostname);

        $alimento = $m->dameAlimento($id);

        $params = $alimento;

        require __DIR__ . '/templates/verAlimento.php';
    }

    public function salir() {
        session_start();
        $_SESSION['SesionValida'] = 0;
        session_destroy();
        header("Location: index.php");
    }

}

?>
