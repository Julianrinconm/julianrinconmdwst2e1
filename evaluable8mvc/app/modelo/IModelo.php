<?php

interface IModelo {

    public function instalar();

    public function calculaidmaxenlaces();

    public function calculaidmaxtipoenlaces();

    public function getEnlaces();

    public function grabarEnlace($enlace);
}
