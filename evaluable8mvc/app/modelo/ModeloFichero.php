<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');


require_once __DIR__ . '/IModelo.php';
require_once __DIR__ . '/Enlaces.php';
require_once __DIR__ . '/TipoEnlaces.php';

class ModeloFichero implements IModelo {

    private $fp = "Enlaces.txt";
    private $fa = "TipoEnlaces.txt";

    //**************************************
    public function getEnlaces() {
        $enlaces = array();

        $f = @fopen($this->fp, "r");

        if ($f) {
            $data = fgetcsv($f, 1000, ";");
            $cont = 0;
            while ($data) {
                $enlace = new Enlaces($data[0], $data[1]);
                $enlaces[$cont] = $enlace;
                $cont++;
                $data = fgetcsv($f, 1000, ";");
            }
            fclose($f);

            //echo "Num prof " . count($enlaces) . "<br>";
            //print_r($enlaces);
        } else {
            echo "Error: No se puede abrir: " . $fp . "<br>";
        }

        return $enlaces;
    }

    //**************************************
    public function getTipoEnlace($enlace_) {
        $f = fopen($this->fp, "r");
        $data = fgetcsv($f, 1000, ";");
        $enlace = new TipoEnlaces("", "");

        while ($data) {
            $idp = $data[0];
            //echo "Prof : " . $idp . " ==  " . $profesores_->getId() . "?<br>";
            if ($idp == $profesores_->getId()) {
                $profesores->setId($data[0]);
                $profesores->setNombre($data[1]);
                break;
            }

            $data = fgetcsv($f, 1000, ";");
        }
        fclose($f);
        //echo "Leido Objeto: " . $profesores->getId() . " " . $profesores->getNombre() . "<br>";
        return $profesores;
    }

    //**************************************
    public function grabarProfesor($profesores) {

        $f = fopen($this->fp, "a");
        $linea = $profesores->getId()
          . ";" . $profesores->getNombre()
          . "\r\n";
        fwrite($f, $linea);

        fclose($f);
    }

    //**************************************
    function grabarEnlace($enlace) {

        $fe = "Enlaces.txt";
        $f = fopen($fe, "a");
        $linea = $enlace->getId()
          . ";" . $enlace->getNombre()
          . ";" . $enlace->getUrl()
          . ";" . $enlace->getTipoenlace()->getId()
          . "\r\n";
        fwrite($f, $linea);

        fclose($f);
    }

    //**************************************

    public function grabarAsignatura($asignatura) {
        $f = fopen($this->fa, "a");
        $linea = $asignatura->getId()
          . ";" . $asignatura->getNombre()
          . ";" . $asignatura->getHoras()
          . ";" . $asignatura->getProfesor()->getId()
          . "\r\n";
        fwrite($f, $linea);
        fclose($f);
    }

    //**************************************
    public function getAsignaturas() {
        $asignaturas = array();
        $f = fopen($this->fa, "r");

        if ($f) {
            $data = fgetcsv($f, 1000, ";");
            $cont = 0;
            while ($data) {
                $profesores = new Profesor($data[2], "");
                $asignatura = new Asignatura($data[0], $data[1], $data[2], $profesores);
                $data = fgetcsv($f, 1000, ";");
                $asignaturas[$cont] = $asignatura;
                $cont++;
            }
            //echo "Asignaturas " . count($asignaturas);
            fclose($f);
        } else {
            echo "Error: No se puede abrir: " . $this->fa . "<br>";
        }

        return $asignaturas;
    }

    public function instalar() {

        // Borramos los ficheros.
        unlink($fa);
        unlink($fp);
    }

    public function calculaidmaxenlaces() {

        $f = fopen($this->fp, "r");
        if (!$f) {
            return 1;
        }
    }

    public function calculaidmaxtipoenlaces() {
        $f = fopen($this->fa, "r");
        if (!$f) {
            return 1;
        }
    }

}
