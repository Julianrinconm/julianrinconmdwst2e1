<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');


require_once __DIR__ . '/IModelo.php';
require_once __DIR__ . '/Enlaces.php';
require_once __DIR__ . '/TipoEnlaces.php';

/**
 * Description of ModeloMysql
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
class ModeloMysql implements IModelo {

    protected $conexion;

    public function __construct() {


        $this->conexion = NULL;

        try {

            $bdconexion = new PDO('mysql:host=' . Config::$bdhostname . ';dbname='
              . Config::$bdhostname . ';charset=utf8', Config::$dbuser, Config::$dbpass);

            $this->conexion = $bdconexion;
        } catch (PDOException $e) {

            echo 'ERROR: ' . $e->getMessage();
        }
    }

    public function getEnlaces() {

        $enlaces = array();
        $consulta = "SELECT id, nombre, url, tipoenlace "
          . "FROM enlaces as a ORDER BY a.id";
        //echo $consulta;

        $result = $this->conexion->query($consulta);

        $cont = 0;
        //print_r($result);
        $filas = array();

        $filas = $result->fetchAll(PDO::FETCH_OBJ);
        //$filas = $result->fetchAll();
        if (!$filas) {
            die("Execute query error, because: " . print_r($this->conexion->errorInfo(), true));
        } else {

            foreach ($filas as $fila) {
                //echo $fila->id . "<br>";
                $tipoenlace = new TipoEnlace($fila->tipoenlace, "");
                //$enlace = new Enlaces($fila->id, $fila->nombre, $fila->url, $tipoenlace);
                $enlaces [$cont] = new Enlaces($fila->id, $fila->nombre, $fila->url, $tipoenlace);
                //$enlaces [$cont] = new Enlaces($fila->id, $fila->nombre, $fila->url);
                $cont++;
            }

            $conexion = false;
        }
        return $enlaces;
    }

    function grabarEnlace($enlace) {


        $consulta = 'INSERT INTO enlaces ( id, nombre, url, tipoenlace ) '
          . 'VALUES( :id,  :nombre, :url, :tipoenlace);';

        //echo $consulta . "<br\>";
        //print_r($enlace);
        echo "<br\>";
        echo "Grabando: " . $enlace->getId() . " " . $enlace->getNombre()
        . " " . $enlace->getUrl() . " " . $enlace->getTipoenlace()->getId();

        $result = $this->conexion->prepare($consulta);

        if ($result) {
            echo "<br>";
            //echo "\nPDO::errorInfo():<br>\n";
            //print_r($result);
            //print_r($this->conexion);
        }


        /* Ejecuta una sentencia preparada pasando un array de valores */
        $count = $result->execute(array(
          ":id" => $enlace->getId()
          , ":nombre" => $enlace->getNombre()
          , ":url" => $enlace->getUrl()
          , ":tipoenlace" => $enlace->getTipoenlace()->getId()
          )
        );

        //echo "Count: " . $count . "<br>";
        //print_r($result->errorInfo());

        $this->conexion = false;

        if ($count == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function calculaidmaxenlaces() {
        // Calcula el id max de profesor

        try {
            $consulta = 'select max(id) as max from enlaces;';
            $query = $this->conexion->query($consulta);
            $fila = $query->fetch();
            $id = $fila["max"];
            //print_r($fila);
            if (!$id) {
                $id = 0;
            }
            //echo $consulta . "<br>";
            //echo $id . "<br>";
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            return 0;
        }
        return $id + 1;
    }

    // Calcula el id max de profesor
    public function calculaidmaxtipoenlaces() {

        // Calcula el id max de profesor

        try {
            $consulta = 'select max(id) as max from tipoenlace;';
            $query = $this->conexion->query($consulta);
            $fila = $query->fetch();
            $id = $fila["max"];

            if (!$id) {
                $id = 0;
            }
            //print_r($fila);
            //echo $consulta . "<br>";
            //echo $id . "<br>";
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            return 0;
        }
        return $id + 1;
    }

    public function grabarProfesor($profesor) {


        try {
            $consulta = 'INSERT INTO profesor (id, nombre) VALUES (:id, :nombre) ;';
            $result = $this->conexion->prepare($consulta);

            if (!$result) {
                //echo "\nPDO::errorInfo():<br>";
                //print_r($this->conexion->errorInfo());
            }

            $count = $result->execute(array(
              ":id" => $profesor->getId()
              , ":nombre" => $profesor->getNombre()));

            //echo $consulta . "<br\>";
            //echo "Count: " . $count . "<br\>";
            //print_r($result);
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            return false;
        }

        $conexion = false;
        if ($count == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function instalar() {

        echo "<h2>Instalando: " . Config::$modelo . "</h2>";
        try {
            // Conectamos sin indicar bbdd
            $conexion = new PDO("mysql:host=" . Config::$bdhostname . ".", Config::$bdusuario, Config::$bdclave);
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            return;
        }


        //creamos la base de datos si no existe
        try {
            $crear_bd = $conexion->prepare('CREATE DATABASE IF NOT EXISTS ' . Config::$bdnombre . ' COLLATE utf8_spanish_ci');
            $crear_bd->execute();
            echo "Creada BDDD: " . Config::$bdnombre . "<br>";
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            return;
        }


        //decimos que queremos usar la BBDD que acabamos de crear
        try {
            $use_db = $conexion->prepare('USE ' . Config::$bdnombre);
            $use_db->execute();
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            return;
        }


        try {
            //creamos la tabla profesor
            $sql = 'CREATE TABLE IF NOT EXISTS profesor (
            id int(3) NOT NULL,
            nombre varchar(80) COLLATE utf8_spanish_ci,
            PRIMARY KEY (id));';

            //echo $sql . "<br>";

            $crear_tb_profesor = $conexion->prepare($sql);
            $crear_tb_profesor->execute();
            echo "Creada tabla: profesor<br>";
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            return;
        }

        try {
            $sql = 'CREATE TABLE IF NOT EXISTS asignatura (
                id int(11) NOT NULL ,
		nombre varchar(100) COLLATE utf8_spanish_ci NOT NULL,
                horas int(3) NOT NULL,
                idp int(11) NOT NULL,
		PRIMARY KEY (id)
                );';
            //echo $sql . "<br>";
            $crear_tb_asignatura = $conexion->prepare($sql);
            $crear_tb_asignatura->execute();
            echo "Creada tabla: asignatura<br>";
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            return;
        }

        try {
            $sql = "ALTER TABLE asignatura ADD CONSTRAINT fk_user_id FOREIGN KEY (idp) references profesor(id);";
            $fk = $conexion->prepare($sql);
            $fk->execute();
            echo "Creada CAj: Asignatura.idp -> Profesor.id <br>";
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
            return;
        }

        $conexion = false;
    }

}
