<?php include_once("funciones.php"); ?>
<!DOCTYPE html>
<html>
    <head>
        <title> <?php echo titulo(); ?></title>
        <meta charset="UTF-8">
    </head>
    <body>

        <?php cabecera(); ?>
        <h3>FORMULARIOS</h3>

        <p>Elegir:</p>
        <ul>
            <li><a href="MenuE.php">Profesor. Gestión de enlaces</a> </li>
            <li><a href="MenuTE.php">Alumno1. Gestión de tipos de enlaces </a> </li>
            <li><a href="MenuU.php">Alumno2. Julian Rincon. Gestión de usuarios</a> </li>
            <li><a href="MenuTU.php">Alumno3. Gestión de tipos de usuarios</a> </li>
        </ul>

        <p>Documentación por tema:</p>
        <ul><li><a href="https://docs.google.com/document/d/1tJ1b7lB0pQUWYSJULCa55Rehc9WbI3Ouqq85SHG_zkg/edit?usp=sharing"  target="docu3">Doc</a> </li>
        </ul>

        <p><a href="../index.php">Atras</a> </p>
        <?php pie(); ?>

    </body>
</html>
