<?php
include_once('Config.php');
include_once("funciones.php");
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title> <?php echo titulo(); ?></title>
    </head>
    <body>



        <?php cabecera(); ?>

        <h3>Resultado del alta</h3>
        <h4>Autor Alumno2: Julian Rincon</h4>

        <?php     
        $id = recoge("id");
        if ($id == "") {
            $id = "Error: Vacio";
        }
        $nombre = recoge("nombre");
        if ($nombre == "") {
            $nombre = "Error: Vacio";
        }

        $tipousuario = recoge("tipousuario");
        if ($tipousuario == "") {
            $tipousuario = "Error: Vacio";
        }

        $usuario = recoge("usuario");
        if ($usuario == "") {
            $usuario = "Error: Vacio";
        }

        $password = recoge("password");
        if ($password == "") {
            $password = "Error: Vacio";
        }

        $email = recoge("email");
        if ($email == "") {
            $email = "Error: Vacio";
        }
        ?>

        <table border="1">
            <tr>
                <td>Id</td>
                <td>
                    <?php echo $id; ?>
                </td>
            </tr>
            <tr>
                <td>Nombre</td>
                <td><?php echo $nombre; ?></td>
            </tr>
            <tr>
                <td>Tipo de Usuario</td>
                <td><?php echo $tipousuario; ?></td>
            </tr>
            <tr>
                <td>Usuario</td>
                <td><?php echo $usuario; ?></td>
            </tr>
            <tr>
                <td>Password</td>
                <td><?php echo $password; ?></td>
            </tr>
            <tr>
                <td>Email</td>
                <td><?php echo $email; ?></td>
            </tr>

        </table>

        <p><a href="MenuU.php">Atras</a> </p>
        <?php pie(); ?>

    </body>
</html>
