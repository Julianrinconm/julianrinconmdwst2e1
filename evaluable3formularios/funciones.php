<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');
include_once('Config.php');

function recoge($var) {
    $tmp = (isset($_REQUEST[$var])) ? strip_tags(trim(htmlspecialchars($_REQUEST[$var], ENT_QUOTES, "UTF-8"))) : "";
    return $tmp;
}

function cabecera() {
    echo "<h1>" . TITULO . "</h1>\n";
    echo "<h2>" . MODULO . " TEMA " . TEMA . " GRUPO " . GRUPO . "  </h2><hr/>\n";
}

function titulo() {
    echo MODULO . " TEMA " . TEMA . " GRUPO" . GRUPO;
}

function pie() {
    echo "<hr/><pre>" . EMPRESA . "  " . MODULO . " TEMA " . TEMA . " GRUPO" . GRUPO . " ";
    echo CURSO . "</pre>\n";
}

function inicio() {
    echo '<align="right"><a href = "index.php" target="inicio" >Inicio</a> </align>';
}

function grupo() {

    echo "<p>Datos del grupo:</p>";

    echo "<table border='1'>";
    echo "<tr>";
    echo "<th>Alumno#</th>";
    echo "<th>Nombre</th>";
    echo "<th>Entregado</th>";
    echo "<th>Bitbucket</th>";
    echo "<th>Heroku</th>";

    echo "</tr>";

    echo "<tr>";
    echo "<td>Profesor</td>";
    echo "<td>Paco Aldarias</td>";

    echo "<td> 14/10/2017</td>";
    echo "<td>";
    echo '<a href="https://bitbucket.org/pacoaldarias/pacoaldariasdwst3e1" target="tema3p">' . 'Bitbucket</a>';
    echo "</td>";
    echo "<td>";
    echo '<a href ="https://pacoaldariasdwst3e1.herokuapp.com" target="tema3a1">' . 'UrlHeroku</a>';
    echo "</td>";



    echo "</tr>";

    echo "<tr>";
    echo "<td>Alumno1</td>";
    echo "<td>Nombre Alumno1</td>";

    echo "<td></td>";
    echo "<td>";
    echo '<a href="https://bitbucket.org/x/xdwst3e1" target="tema2b1">' . 'Bitbucket</a>';
    echo "</td>";

    echo "<td>";
    echo '<a href ="https://xdwst3e1.herokuapp.com/" target="tema3h1">' . 'UrlHeroku</a>';
    echo "</td>";


    echo "</tr>";

    echo "<tr>";
    echo "<td>Alumno2</td>";
    echo "<td>Nombre Alumno2</td>";

    echo "<td></td>";
    echo "<td>";
    echo '<a href="https://bitbucket.org/x/xdwst2e1" target="tema2b2">' . 'Bitbucket</a>';
    echo "</td>";
    echo "<td>";
    echo '<a href ="https://xdwst2e1.herokuapp.com/" target="tema2h2">' . 'UrlHeroku</a>';
    echo "</td>";

    echo "</tr>";

    echo "<tr>";
    echo "<td>Alumno3</td>";
    echo "<td>Nombre Alumno3</td>";

    echo "<td></td>";
    echo "<td>";
    echo '<a href="https://bitbucket.org/pacoaldarias/xdwst2e1" target="tema2b3">' . 'Bitbucket</a>';
    echo "</td>";

    echo "<td>";
    echo '<a href ="https://xdwst2e1.herokuapp.com/" target="tema2h3">' . 'UrlHeroku</a>';
    echo "</td>";



    echo "</tr>";
    echo "</table>";
}

function docu() {

    echo "<p>Documentación</p>";
    echo "<ul>";
    echo "<li>";
    echo '<a href ="https://docs.google.com/document/d/1Z6xuT5Q0BGdhK6hT7rvboaKAdCM26iZ1LUpOm9De0lI/edit?usp=sharing" target="gdtema3h1">' . 'GoogleDocs Enunciado Evaluable3</a>';
    echo "</li>";
    echo "<li>";
    echo '<a href ="https://docs.google.com/document/d/1Z6xuT5Q0BGdhK6hT7rvboaKAdCM26iZ1LUpOm9De0lI/edit?usp=sharing" target="gdtema3h1">' . 'GoogleDocs del Grupo</a>';
    echo "</li>";
    echo "</ul>";
}
?>

