drop table enlaces;
drop table tipoenlace;

CREATE TABLE tipoenlace (
   id int(11) NOT NULL ,
   nombre varchar(100)  NOT NULL,
   CONTRAINT pk_tipoenlace PRIMARY KEY (id)
);
insert into tipoenlace(id,nombre) values (1,"Educacion");
insert into tipoenlace(id,nombre) values (2,"Buscador");



CREATE TABLE enlaces (
        id int(3) ,
         nombre varchar(80),
         url varchar(80),
        tipoenlace int(3),
        CONSTRAINT pk_enlaces PRIMARY KEY (id),
	    CONSTRAINT fk_enlace_tipoenlace FOREIGN KEY(tipoenlace) REFERENCES tipoenlace(id)
);
insert into enlaces(id,nombre, url, tipoenlace) values (1,"ceed","http://www.ceedcv.es",1);
insert into enlaces(id,nombre, url, tipoenlace) values (2,"google","http://www.google.com",1);


CREATE TABLE tipousuario (
            id int(3) ,
            nombre varchar(80),
            CONSTRAINT pk_tipousuario PRIMARY KEY (id)
);

CREATE TABLE usuario (
            id int(3) ,
            nombre varchar(80),
            usuario varchar(80),
            password varchar(80),
	    tipousuario int(3),
            CONSTRAINT pk_usuario PRIMARY KEY (id),
	    CONSTRAINT fk_usuario_tipousuario FOREIGN KEY(tipousuario) REFERENCES tipousuario(id)
);

insert into usuario(id,nombre,usuario,password, tipousuario) values (1,"Paco Aldarias","paco", "123",1);
insert into usuario(id,nombre,usuario,password, tipousuario)  values (2,"Juan Garcia","juan", "123",2);

insert into tipousuario(id,nombre) values (1,"Profesor");
insert into tipousuario(id,nombre) values (2,"Alumno");



insert into tipoenlace(id,nombre) values (1,"Educacion");
insert into tipoenlace(id,nombre) values (2,"Buscador");


			


