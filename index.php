<!DOCTYPE html>
<html>
    <head>
        <title>DAW2 DWS.</title>
        <meta charset="UTF-8">
    </head>
    <body>

        <h1>APLICACIÓN WEB</h1>
        <h2>DWS. GRUPO 1</h2>

        <hr/>

        <p>Inicio</p>

        <p>Elegir:</p>
        <ul>
            <li><a href="evaluable2php/index.php">Tema 2. Php</a> </li>
            <li><a href="evaluable3formularios/index.php">Tema 3. Formularios</a> </li>
            <li><a href="evaluable4ficheros/index.php">Tema 4. Ficheros</a> </li>
            <li><a href="evaluable5objetos/index.php">Tema 5. Objetos</a> </li>
            <li><a href="evaluable6mysql/index.php">Tema 6. BBDD</a> </li>
            <li><a href="evaluable7sesiones/sesiones.php">Tema 7. Sesiones</a> </li>
              <!-- <li><a href="evaluable8mvc/web/index.php">Tema 8. MVC</a> </li> -->
            <li><a href="evaluable9mashup/index.php">Tema 9. Mashup</a> </li>
            <!-- <li><a href="evaluable10laravel/pacoaldariasdwslaravel/public/index.php">Tema 10. Larabel</a> </li> -->
            <li><a href="evaluable10laravel/pacoaldariasdwslaravel/public/index.php">Tema 11. Larabel</a> </li>

        </ul>

        <p>Datos del grupo:</p>

        <table border='1'>
            <tr>
                <th>Alumno#</th>
                <th>Nombre</th>
                <th>Bitbucket</th>
                <th>Heroku</th>
            </tr>

            <tr>
                <td>Profesor</td>
                <td>Paco Aldarias</td>

                <td>
                    <a href="https://bitbucket.org/pacoaldarias/pacoaldariasdws" target="tema2p">Bitbucket</a>
                </td>
                <td>
                    <a href ="https://pacoaldariasdws.herokuapp.com" target="heroku">Heroku</a>
                </td>
            </tr>

            <tr>
                <td>Alumno1</td>
                <td>Beatriz Planells</td>


                <td>
                    <a href="https://bitbucket.org/bplanells/bplanellsdwst2e1" target="tema2b1">Bitbucket</a>
                </td>
                <td>
                    <a href ="https://bplanellsdws.herokuapp.com/" target="tema2h1">Heroku</a>
                </td>
            </tr>

            <tr>
                <td>Alumno2</td>
                <td>Julian Rincon</td>


                <td>
                    <a href="https://bitbucket.org/julianrinconm/julianrinconmdwst2e1" target="tema2b2">Bitbucket</a>
                </td>
                <td>
                    <a href ="https://julianrinconmdwst2e1.herokuapp.com/" target="tema2h2">Heroku</a>
                </td>
            </tr>

            <tr>
                <td>Alumno3</td>
                <td>Mercedes Toledo</td>


                <td>
                    <a href="https://bitbucket.org/mercedestoledo/mercedestoledodwst2e1" target="tema2b3">Bitbucket</a>
                </td>

                <td>
                    <a href ="https://mercedestoledodws.herokuapp.com/" target="tema2h3">Heroku</a>
                </td>


            </tr>
        </table>




        <ul>
            <li>
                <a href ="https://docs.google.com/document/d/1nLR33H09fg0bjFhniBq58qUDgM-Z7qmy8JOMwi9ZAu8/edit" target="docgru">Documentación del grupo</a>
            </li>
        </ul>

    </p>

    <hr/><pre> CEEDCV  DAW2 DWS GRUPO2 </pre>

</body>
</html>

