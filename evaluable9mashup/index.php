<?php include_once("funciones.php"); ?>
<!DOCTYPE html>
<html>
    <head>
        <title> <?php echo titulo(); ?></title>
        <meta charset="UTF-8">
    </head>
    <body>
        <?php cabecera(); ?>
        <h3>TEMA 9. MASHUP</h3>
        <p>Lista de grupos y tags de dadesobertes de la Generalitat Valenciana</p>
        <a href='http://www.dadesobertes.gva.es'> DadesObertes </a>
        <br>
       <table border="1">    
       <?php
        $url='http://www.dadesobertes.gva.es/api/3/action/group_list';
        $json = file_get_contents($url);
        $data = json_decode($json,TRUE);
        // No necesito la ayuda, bueno, si la necesitaba, pero ahora ya no
        unset($data["help"]);
        // Tampoco quiero saber si ha funcionado, mas me vale que sí
        // En un futuro puedo usarlo para control de errores
        unset($data["success"]);
        // Solo me interesa el array result, pero bueno, ya he borrado el resto 
            echo "<th>Grupos</th>";
            foreach($data['result'] as $celda){
                echo "<tr><td>";    
                echo "$celda";
                echo "</td></tr>"; 
            }
            echo "</tr>";
        $url='http://www.dadesobertes.gva.es/api/3/action/tag_list';
        $json = file_get_contents($url);
        $data = json_decode($json,TRUE);    
        // Sigo sin necesitar la ayuda
        unset($data["help"]);
        // El control de errores para otro año
        unset($data["success"]);
        echo "<th>Tags</th>";
        foreach($data['result'] as $celda){
            echo "<tr><td>";    
            echo "$celda";
            echo "</td></tr>"; 
        }
        echo "</tr>";
?>
 </table>
        <p>Enlaces:</p>
        <ul>
        <li>
        Documentación de esta práctica:
        <a href="DWST9P8DATOSABIERTOS.odt"  target="datosabiertos">odt</a> 
        </li>

        <li>
        Enunciado práctica evaluable:
        <a href="https://docs.google.com/document/d/1MvX9pSwWiBkojAs7KguauT_85H-HEmvyB-A9UiMY1sA/edit?usp=sharing"  target="docu5">Evaluable 9</a> 
        </li>

        </ul>

        <?php volver(); ?>
        <?php pie(); ?>

    </body>
</html>
